<?php

get_header();

?>

<div class="clear"></div>

<div class="inside-page row">

    <div class="c cx3">
    
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
            ?>
            
            <div class="single-subtitle">
                <div class="floatleft">
                    <h1><?php the_title(); ?></h1>
                    <div class="h1bar">&nbsp;</div>
                </div>
                <div class="floatright" style="margin-top: 5px;"><ul>
                    <li><a href="mailto:?subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>"><img src="<?php echo images(); ?>email-icon.jpg" alt="Email"/></a></li>
                    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&summary=<?php the_title(); ?>" target="_blank"><img src="<?php echo images(); ?>linkedin-icon.jpg" alt="Linked In"/></a></li>
                    <li><a href="http://www.facebook.com/sharer.php?s=100&p[title]=<?php echo urlencode(str_replace('&#038;', '&', get_the_title()));
                        
                        if (!empty($features))
                        {
                          ?>&p[summary]=<?php echo urlencode(str_replace('&#038;', '&', $features));
                          
                        }
                        ?>&src=sp" target="_blank"><img src="<?php echo images(); ?>facebook-icon.jpg" alt="Facebook"/></a></li>
                        
                    <li><a href="http://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>" target="_blank"><img src="<?php echo images(); ?>twitter-icon.jpg" alt="Twitter"/></a></li>
                    <?php /*
                    <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><img src="<?php echo images(); ?>gplus-icon.jpg" alt="Google Plus"/></a></li> */ ?>
                </ul></div>
            </div>
            
            <div class="clear"></div>
            
            <?php the_content(); ?>
            
            <?php
        endwhile;
        ?>
    
    </div>
    
    <div class="c cx2">
        
        <div class="more-info-block">
            
            <div class="head">
                <div class="top-left floatleft">
                    <div></div>
                </div>
                <div class="top-center floatleft">
                    <img src="<?php images(); ?>envelope.png" alt="">
                </div>
                <div class="top-right floatleft">
                    <div></div>
                </div>
                <div class="clear"></div>
            </div>
            
            <div class="clear"></div>
            
            <div class="center uppercase bold pink" style="margin-bottom: 20px;">Preferred Realtor<sup>&reg;</sup> registration</div>
            
            <div class="clear"></div>
            
            <?php echo FrmFormsController::get_form_shortcode(array('id' => 8, 'title' => false, 'description' => false)); ?>
            
        </div>
    </div>

    <div class="clear"></div>
    
</div>

<?php

get_footer();

?>