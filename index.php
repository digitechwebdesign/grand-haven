<?php
/*
Template Name: Front Page
*/

get_header();

?>

<div class="row">
    <div class="c cx8"><?php
        //do_action('slideshow_deploy', '57');
        the_content();
    ?></div>
    <div class="clear"></div>
</div>

<div class="row">
    <?php
        
        $args = array(
            'post_type'         => 'frontblocks',
            'posts_per_page'    => 4
        );
        
        $query = new WP_Query( $args );
        while ( $query->have_posts() ) {
            $query->the_post();
            
            $block_image = get_field("block_image");
            $alt_text = get_field("alt_text");
            $block_link = get_field("link");
            
            ?>
            
            <div class="c cx2"><a href="<?php echo $block_link; ?>"><img src="<?php echo $block_image; ?>" alt="<?php echo $alt_text; ?>"/></a></div>
            
            <?php
            
            // End the loop.
        }
        
        wp_reset_query();
        
    /*
    <div class="c cx2"><a href="/contact-us/"><img src="<?php images(); ?>/find-new-home.jpg" alt="Find your new home!"/></a></div>
    <div class="c cx2"><a href="/our-communities/northwoods-at-blanco-vista/"><img src="<?php images(); ?>/explore-community.jpg" alt="Explore a community"/></a></div>
    <div class="c cx2"><a href="/about-us/"><img src="<?php images(); ?>/about-us.jpg" alt="About us"/></a></div>
    <div class="c cx2"><a href="/realtors/"><img src="<?php images(); ?>/for-realtors.jpg" alt="For realtors"/></a></div>
    */ ?>
    <div class="clear"></div>
</div>

<div class="row">
    <div id="sign-up" class="c cx8">
        <div id="lets-connect" class="floatleft">
            <div class="floatleft envelope"><img src="<?php images(); ?>envelope.png" alt=""></div>
            <div class="floatleft lc-block"><span class="pink bold">LET'S CONNECT.</span></div>
            <div class="clear"></div>
        </div>
        <div id="connect-form" class="floatright form">
            
            <?php echo FrmFormsController::get_form_shortcode(array('id' => 6, 'title' => false, 'description' => false)); ?>
            
            <script>
                $(function() {
                    $("#field_rxvbfh").focusin(function() {
                        if ($("#field_rxvbfh").val() == "First Name") {
                            $("#field_rxvbfh").val("");
                        }
                    });
                    
                    $("#field_ezmcmj").focusin(function() {
                        if ($("#field_ezmcmj").val() == "Email Address") {
                            $("#field_ezmcmj").val("");
                        }
                    });
                    
                    $("#field_rxvbfh").focusout(function() {
                        if ($("#field_rxvbfh").val() == "") {
                            $("#field_rxvbfh").val("First Name");
                        }
                    });
                    
                    $("#field_ezmcmj").focusout(function() {
                        if ($("#field_ezmcmj").val() == "") {
                            $("#field_ezmcmj").val("Email Address");
                        }
                    });
                });
            </script>
            
            <?php /*
            <input id="frm-first-name" type="text" value="First Name"/>
            <input id="frm-email" type="text" value="Email"/>
            <input type="button" value="Send"/>
            
            <script>
                $(function() {
                    $("#frm-first-name").focusin(function() {
                        if ($("#frm-first-name").val() == "First Name") {
                            $("#frm-first-name").val("");
                        }
                    });
                    
                    $("#frm-email").focusin(function() {
                        if ($("#frm-email").val() == "Email") {
                            $("#frm-email").val("");
                        }
                    });
                    
                    $("#frm-first-name").focusout(function() {
                        if ($("#frm-first-name").val() == "") {
                            $("#frm-first-name").val("First Name");
                        }
                    });
                    
                    $("#frm-email").focusout(function() {
                        if ($("#frm-email").val() == "") {
                            $("#frm-email").val("Email");
                        }
                    });
                });
            </script>
            */ ?>
        </div>
    </div>
    <div class="clear"></div>
</div>

<div class="row">
    <div class="c cx4">
        <div id="hey-there-banner"><h3><span class="playfair">FALL IN LOVE.</span> <span class="lato">AGAIN.</span><h3></div>
        <div class="pad" style="line-height: 1.500em;">
            <b>FEEL</b> the <em>flutter</em>
            of excitement when you find the new home that's meant for you.
            A home that's smart, green and designed with custom touches that make it much <em>more</em> you.
            Our homes are tucked into vibrant Central Texas communities, surrounded by shops,
            parks, neighbors and friends. See why Brookfield Residential truly offers the best places to call home.

            <br /><br />
            <h3><span class="playfair">Blanco Vista: Four Model Homes Open Now!</span></h3>
            Our exciting model home park that invites you to experience a unique tour of four distinctly styled homes designed for how you live today.
            You can choose both the floor plan <em>and</em> the exterior you like. After all, when it comes to love, why compromise?
            
            <br/><br/>
            <h3><span class="playfair">Grand Haven Homes: Now Part of Brookfield Residential!</span></h3>
            Brookfield Residential recently acquired <a href="http://www.grandhavenhomes.net/" target="_blank">Grand Haven Homes</a>,
            which includes 15 distinctive communities across Central Texas.
            <a href="http://www.grandhavenhomes.net/" target="_blank">Explore Grand Haven</a> and see why we fell in love with this gorgeous collection of homes and communities. 
            
            <div id="bptch-block" class="pad">
                <div></div>
                See why <a href="/about-us/" class="lato bold">BROOKFIELD RESIDENTIAL</a> truly offers<br/>
                the <span style="font-family: Georgia, Serif;"><em>best places to call home</em></span>.<br/>
                <img src="<?php echo images(); ?>curly-mark.jpg" alt=""/>
            </div>
        </div>
    </div>
    <div class="c cx4">
        <div id="find-new-homes"><h3><span class="playfair">FIND A NEW HOME</span> <span class="lato">in one of OUR great communities.</span><h3></div>
        <div class="pad-top">
            <div class="iframe-container">
                <div id="map-canvas"></div>
                <?php
                // Northwoods: 29.9460222,-97.89691470000002
                // Dripping springs: 30.195702,-98.0960103
                
                //<iframe src="https://www.google.com/maps/d/embed?mid=z9ZAQHt8IGhw.kFB8RFNGvJMc" width="100%" height="370" frameborder="0" style="border:0"></iframe>
                ?>
                
                
                <script language="javascript">
                    
                    var northwoods = new google.maps.LatLng(29.9460222, -97.89691470000002);
                    var dripping_springs = new google.maps.LatLng(30.195702, -98.0960103);
                    var marker1, marker2;
                    var map;
                    
                    function initialize() {
                        var mapOptions = {
                            center: northwoods,
                            zoom: 8
                        };
                        
                        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                        
                        marker1 = new google.maps.Marker({
                            map:map,
                            draggable:false,
                            position: northwoods,
                            title: "Northwoods at Blanco Vista"
                        });
                        
                        marker2 = new google.maps.Marker({
                            map:map,
                            draggable:false,
                            position: dripping_springs,
                            title: "Retreat at Dripping Springs"
                        });
                        
                        var marker1_content = '<div class="content">'+
                            '<h2>Northwoods at Blanco Vista</h2><p>Four model homes opening summer 2015.</p>'+
                            '<div><a href="/communities/northwoods-at-blanco-vista/" target="_parent">Visit this community</a></div>'+
                            '</div>';
                            
                        var infowindow1 = new google.maps.InfoWindow({
                            content: marker1_content
                        });
                        
                        var marker2_content = '<div class="content">'+
                            '<h2>Retreat at Dripping Springs</h2><p>Model homes under construction summer 2015.</p>'+
                            '<div><a href="/communities/retreat-at-dripping-springs/" target="_parent">Visit this community</a></div>'+
                            '</div>';
                            
                        var infowindow2 = new google.maps.InfoWindow({
                            content: marker2_content
                        });
                        
                        google.maps.event.addListener(marker1, 'click', function() {
                            infowindow1.open(map, marker1);
                        });
                        
                        google.maps.event.addListener(marker2, 'click', function() {
                            infowindow2.open(map, marker2);
                        });
                    }
                    
                    google.maps.event.addDomListener(window, 'load', initialize);
                </script>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>

<?php

get_footer();

?>