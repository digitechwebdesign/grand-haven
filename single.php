<?php

get_header();

?>

<div class="clear"></div>

<div class="inside-page row">

    <div class="c cx3">
    
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
            $slideshow = get_field("slideshow");
            $thumbnail = get_field("thumbnail");
            $floor_plan = get_field("floor_plan");
            $fact_sheet = get_field("fact_sheet");
            $price = get_field("price");
            $square_feet = get_field("square_feet");
            $bedrooms = get_field("bedrooms");
            $full_baths = get_field("full_baths");
            $garages = get_field("garages");
            $map_location = get_field("map_location");
            $features = get_field("features");
            $community = get_field("community");
            
	    $this_post_id = get_the_ID();
            
            ?>
            
            <h1><?php the_title(); ?></h1>
            <div class="single-subtitle">
                <div class="floatleft">
                    <?php echo $square_feet; ?> SQUARE FEET,
                    <?php echo $bedrooms; ?> BEDROOMS,
                    <?php echo $full_baths; ?> BATHS, 
                    <?php echo $garages; ?> CAR GARAGE
                </div>
                <div class="floatright"><ul>
                    <li><a href="mailto:?subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>"><img src="<?php echo images(); ?>email-icon.jpg" alt="Email"/></a></li>
                    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&summary=<?php the_title(); ?>" target="_blank"><img src="<?php echo images(); ?>linkedin-icon.jpg" alt="Linked In"/></a></li>
                    <li><a href="http://www.facebook.com/sharer.php?s=100&p[title]=<?php echo urlencode(str_replace('&#038;', '&', get_the_title()));
                                
                                if (!empty($features))
                                {
                                  ?>&p[summary]=<?php echo urlencode(str_replace('&#038;', '&', $features));
                                }
                                
                                ?>&src=sp" target="_blank"><img src="<?php echo images(); ?>facebook-icon.jpg" alt="Facebook"/></a></li>
                    <li><a href="http://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>" target="_blank"><img src="<?php echo images(); ?>twitter-icon.jpg" alt="Twitter"/></a></li><?php /*
                    <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><img src="<?php echo images(); ?>gplus-icon.jpg" alt="Google Plus"/></a></li> */ ?>
                    
                </ul></div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            
            <div class="slideshow">
                <?php echo apply_filters('the_content', $slideshow); ?>
            </div>
            
            <div class="single-cta">
                &nbsp;
            </div>
            <div class="cta-icons">
                <?php
                    if($fact_sheet) {
                        ?>
                        
                        <a href="<?php echo $fact_sheet; ?>"><img src="<?php images(); ?>fact-sheet.png"/></a>
                        
                        <?php
                    }
                    
                    if($floor_plan) {
                        ?>
                        
                        <a href="<?php echo $floor_plan; ?>"><img src="<?php images(); ?>floor-plan.png"/></a>
                        
                        <?php
                    }
                ?>
                
                <div class="clear"></div>
            </div>
            
            <div class="contents">
                
                <div class="playfair title bold"><em><?php echo $features; ?></em></div>
                <div class="clear"></div>
                <?php
                    the_content();
                ?>
                
            </div>
            
            <?php
            
        // End the loop.
        endwhile;
        ?>
        
        <div class="clear"></div>
    
    </div>
    
    <div class="c cx2">
        <?php show_homes("Additional Home plans", $this_post_id, $community); ?>
        
        
        <div class="clear"></div>
        
        <div class="more-info-block">
            
            <div class="head">
                <div class="top-left floatleft">
                    <div></div>
                </div>
                <div class="top-center floatleft">
                    <img src="<?php images(); ?>envelope.png" alt="">
                </div>
                <div class="top-right floatleft">
                    <div></div>
                </div>
                <div class="clear"></div>
            </div>
            
            <div class="clear"></div>
        
            <div class="center uppercase bold pink" style="margin-bottom: 20px;">Need more info?</div>
            
            <div class="clear"></div>
            
            <?php echo FrmFormsController::get_form_shortcode(array('id' => 7, 'title' => false, 'description' => false)); ?>
            
        </div>
    </div>

    <div class="clear"></div>
    
</div>

<?php

get_footer();

?>