<?php
/*
Template Name: Reap Landing Page
*/

get_header();

?>
<style>
#form{
float: right; 
width: 30%;
padding-top:20px;
margin-right:40px;
}
#content{
width: 65%; 
float: left;
padding-top:40px;
line-height:2;
}
#main{
max-width:100%;
}
@media screen and (max-width:800px){
#form{
float: none; 
width: 100% !important;
}
#content{
width: 100% !important; 
float: none;
}
input{
width:100% !important;
}
}
</style>
<div class="clear"></div>

<div class="inside-page row">
    <?php
    // Start the loop.
    while ( have_posts() ) : the_post();
        ?>
        
        <h1><?php the_title(); ?></h1>
        
        <?php the_content(); ?>
        
        <?php
    endwhile;
    ?>
</div>

<?php

get_footer();

?>