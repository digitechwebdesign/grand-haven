// Fade-Out Loader
$(document).ready(function() {
	$('.loaderOverlay').fadeOut();

	/*jQuery(".fancy-image").fancybox({
		  helpers: {
			  title : {
				  type : 'float'
			  }
		  }
	  });*/

	jQuery('.fancy-image').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		fixedContentPos: true,
		mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
		image: {
			verticalFit: true
		}
	});


	// Open Gravity Forms (Currently on Grand Haven)
	$('.fancy-form').click(function(){
		jQuery.fancybox({
			href: '#inquire-now',
			maxWidth	: 800,
			fitToView	: true,
			width : '70%'
		});
	});

	// Open Gravity Forms (Currently on Norhtwoods at Blanco Vista)
	$('.home-block').each(function(i) {
		i++;
		$('#open-form' + i).click(function(){
		    jQuery.fancybox({
		        href: '#inquire-now' + i,
		        maxWidth	: 800,
		        fitToView	: false,
		        width : '70%'
		    });
	    });
    });

	// MAP -------------//
	// hover
	$("#btn-bryson").hover(function() { $("#tt-bryson").fadeIn(); }, function() { $("#tt-bryson").fadeOut(); });
	$("#btn-cottages-crystal-falls").hover(function() { $("#tt-cottages-crystal-falls").fadeIn(); }, function() { $("#tt-cottages-crystal-falls").fadeOut(); });
	$("#btn-belterra").hover(function() { $("#tt-belterra").fadeIn(); }, function() { $("#tt-belterra").fadeOut(); });
	$("#btn-caliterra").hover(function() { $("#tt-caliterra").fadeIn(); }, function() { $("#tt-caliterra").fadeOut(); });
	$("#btn-terra-colinas").hover(function() { $("#tt-terra-colinas").fadeIn(); }, function() { $("#tt-terra-colinas").fadeOut(); });
	$("#btn-travisso").hover(function() { $("#tt-travisso").fadeIn(); }, function() { $("#tt-travisso").fadeOut(); });
	$("#btn-cap-rock-estates").hover(function() { $("#tt-cap-rock-estates").fadeIn(); }, function() { $("#tt-cap-rock-estates").fadeOut(); });
	$("#btn-santa-rita-ranch").hover(function() { $("#tt-santa-rita-ranch").fadeIn(); }, function() { $("#tt-santa-rita-ranch").fadeOut(); });
	$("#btn-water-oak").hover(function() { $("#tt-water-oak").fadeIn(); }, function() { $("#tt-water-oak").fadeOut(); });
	$("#btn-rough-hollow").hover(function() { $("#tt-rough-hollow").fadeIn(); }, function() { $("#tt-rough-hollow").fadeOut(); });
	$("#btn-pearson-place").hover(function() { $("#tt-pearson-place").fadeIn(); }, function() { $("#tt-pearson-place").fadeOut(); });
	$("#btn-cimarron-hills").hover(function() { $("#tt-cimarron-hills").fadeIn(); }, function() { $("#tt-cimarron-hills").fadeOut(); });
	$("#btn-cottages-northwoods").hover(function() { $("#tt-cottages-northwoods").fadeIn(); }, function() { $("#tt-cottages-northwoods").fadeOut(); });
	$("#btn-summit-rough-hollow").hover(function() { $("#tt-summit-rough-hollow").fadeIn(); }, function() { $("#tt-summit-rough-hollow").fadeOut(); });
	$("#btn-paloma-lake").hover(function() { $("#tt-paloma-lake").fadeIn(); }, function() { $("#tt-paloma-lake").fadeOut(); });
	$("#btn-cottages-abrantes").hover(function() { $("#tt-cottages-abrantes").fadeIn(); }, function() { $("#tt-cottages-abrantes").fadeOut(); });
	$("#btn-abrantes").hover(function() { $("#tt-abrantes").fadeIn(); }, function() { $("#tt-abrantes").fadeOut(); });
	$("#btn-sarita-valley").hover(function() { $("#tt-sarita-valley").fadeIn(); }, function() { $("#tt-sarita-valley").fadeOut(); });
	// click
	$("#btn-abrantes").click(function() { MapFilter('Abrantes'); });
	$("#btn-belterra").click(function() { MapFilter('Belterra'); });
	$("#btn-bryson").click(function() { MapFilter('Bryson'); });
	$("#btn-caliterra").click(function() { MapFilter('Caliterra'); });
	$("#btn-cap-rock-estates").click(function() { MapFilter('Cap Rock Estates'); });
	$("#btn-cimarron-hills").click(function() { MapFilter('Cimarron Hills'); });
	$("#btn-cottages-abrantes").click(function() { MapFilter('Cottages at Abrantes'); });
	$("#btn-cottages-crystal-falls").click(function() { MapFilter('Cottages at Crystal Falls'); });
	$("#btn-cottages-northwoods").click(function() { MapFilter('Cottages at Northwoods'); });
	$("#btn-paloma-lake").click(function() { MapFilter('Paloma Lake'); });
	$("#btn-pearson-place").click(function() { MapFilter('Pearson Place'); });
	$("#btn-rough-hollow").click(function() { MapFilter('Rough Hollow'); });
	$("#btn-santa-rita-ranch").click(function() { MapFilter('Santa Rita Ranch'); });
	$("#btn-sarita-valley").click(function() { MapFilter('Sarita Valley'); });
	$("#btn-summit-rough-hollow").click(function() { MapFilter('Summit at Rough Hollow'); });
	$("#btn-terra-colinas").click(function() { MapFilter('Terra Colinas'); });
	$("#btn-travisso").click(function() { MapFilter('Travisso'); });
	$("#btn-water-oak").click(function() { MapFilter('Water Oak'); });

	var MapFilter = function(selected) {
		var community = selected;
		$('#community').val(community);
		var $title = $('.thecommunity .dropy_title .title-container span');
		$title.html(community);
		$('.thecommunity .dropy_content a').removeClass('selected');
		filter.init();
	};

	// Dropy Drop-Downs
	/* Built by digiTech */
	var dropy = {
		$dropys: null,
		openClass: 'open',
		selectClass: 'selected',
		init: function(){
			var self = this;

			self.$dropys = $('.dropy');
			self.eventHandler();
		},
		eventHandler: function(){
			var self = this;

			// Opening a dropy
			self.$dropys.find('.dropy_title').click(function(){
				self.$dropys.removeClass(self.openClass);
				$(this).parents('.dropy').addClass(self.openClass);
			});

			// Click on a dropy list
			self.$dropys.find('.dropy_content ul li a').click(function(){
				var $that = $(this);
				var $dropy = $that.parents('.dropy');
				var $input = $dropy.find('input');
				var $title = $(this).parents('.dropy').find('.dropy_title .title-container span');

				// Remove selected class
				$dropy.find('.dropy_content a').each(function(){
					$(this).removeClass(self.selectClass);
				});

				// Update selected value
				if(!$that.hasClass('dropy_header')){
					$title.html($that.html());
					$input.val($that.html());
				}

				// If back to default, remove selected class else addclass on right element
				if($that.hasClass('dropy_header')){
					$title.removeClass(self.selectClass);
					$title.html($title.attr('data-title'));
				}
				else{
					$title.addClass(self.selectClass);
					$that.addClass(self.selectClass);

					// Init Filter
					filter.init();
				}

				// Close dropdown
				$dropy.removeClass(self.openClass);

			});

			// Close all dropdown onclick on another element
			$(document).bind('click', function(e){
			if (! $(e.target).parents().hasClass('dropy')){
				self.$dropys.removeClass(self.openClass); }
			});
			}
	};

	// Grand Haven Filtering
	filter = {
		init: function() {
			// Fade Out Current Results
			$(".home-block").fadeOut();

			// Grab all Current Filter Variables
			var community = $("#community").val();
			var city = $("#city").val();
			var min_price = $("#min-price").val().replace(/,/g, '');
			var max_price = $("#max-price").val().replace(/,/g, '');
			var min_sqft = $("#min-sqft").val();
			var garage = $("#garage").val();
			var bath = $("#bath").val();
			var beds = $("#beds").val();

			// Fix for using a million
			if (max_price == '$1000000') {
				max_price = '$999999';
			};

			var block_total = 0;

			$(".home-block").each(function() {

				var home_price = $(this).find(".content .block-row .results-price").text().replace(/,/g, '');

				// Check home block against all filters
				if (($(this).find(".content .block-row .community").text() == community) || (community == '') || (community == 'Any')) {
					var community_check = 1; // thumbs up
				}
				if (($(this).find(".content .block-row .city").text() == city) || (city == '') || (city == 'Any')) {
					var city_check = 1; // thumbs up
				}
				if ((home_price >= min_price) || (min_price == '') || (min_price == 'Any')) {
					var min_price_check = 1; // thumbs up
				}
				if ((home_price <= max_price) || (max_price == '') || (max_price == 'Any')) {
					var max_price_check = 1; // thumbs up
				}
				if (($(this).find(".content .block-row .results-sqft").text() >= min_sqft) || (min_sqft == '') || (min_sqft == 'Any')) {
					var min_sqft_check = 1; // thumbs up
				}
				if (($(this).find(".content .block-row .results-garage").text() >= garage) || (garage == '') || (garage == 'Any')) {
					var garage_check = 1; // thumbs up
				}
				if (($(this).find(".content .block-row .results-bath").text() >= bath) || (bath == '') || (bath == 'Any')) {
					var bath_check = 1; // thumbs up
				}
				if (($(this).find(".content .block-row .results-beds").text() >= beds) || (beds == '') || (beds == 'Any')) {
					var beds_check = 1; // thumbs up
				}

				// Check if we have thumbs up on all filters
				if ((community_check === 1) && (city_check === 1) && (min_price_check === 1) && (max_price_check === 1) && (min_sqft_check === 1) && (garage_check === 1) && (bath_check === 1) && (beds_check === 1)) {
					block_total += 1;
					$(this).addClass('display-this');
				} else {
					$(this).removeClass('display-this');
				}
			});

			// Show the total number of results
			$('.home-results .title span').html(block_total);
			filter.loadmore(block_total);
		},

		loadmore: function(homes) {
			console.log('Homes Total Pre-Start: ' + homes);

			var displayThis = $(".display-this").length;
			console.log('Display This: ' + displayThis);

			if (typeof homes == 'undefined') {
				var homes = $(".display-this").length;
			};

			console.log('Homes Total Initially: ' + homes);
			x = 8;

			if (displayThis > x) {
				$('.link .load-more').fadeIn();
			} else {
				$('.link .load-more').fadeOut();
			}

			$('.display-this:lt('+x+')').fadeIn();

			$('.load-more').click(function(event) {
				event.preventDefault();
				// console.log('Homes Total: ' + homes);
				// x= (x+8 <= homes) ? x+8 : homes;
				x = x+8
				console.log('X: ' + x);
				$('.display-this:lt('+x+')').fadeIn();

				if (displayThis < x) {
					$('.link .load-more').fadeOut();
				}
			});
		}
	}

	dropy.init();
	filter.loadmore();

	// Confetti Particles
	(function() {
	  var COLORS, Confetti, NUM_CONFETTI, PI_2, canvas, confetti, context, drawCircle, i, range, resizeWindow, xpos;

	  NUM_CONFETTI = 250;

	  COLORS = [[85, 71, 106], [174, 61, 99], [219, 56, 83], [244, 92, 68], [248, 182, 70]];

	  PI_2 = 2 * Math.PI;

	  canvas = document.getElementById("world");

	  context = canvas.getContext("2d");

	  window.w = 0;

	  window.h = 0;

	  resizeWindow = function() {
		window.w = canvas.width = window.innerWidth;
		return window.h = canvas.height = window.innerHeight;
	  };

	  window.addEventListener('resize', resizeWindow, false);

	  window.onload = function() {
		return setTimeout(resizeWindow, 0);
	  };

	  range = function(a, b) {
		return (b - a) * Math.random() + a;
	  };

	  drawCircle = function(x, y, r, style) {
		context.beginPath();
		context.arc(x, y, r, 0, PI_2, false);
		context.fillStyle = style;
		return context.fill();
	  };

	  xpos = 0.5;

	  document.onmousemove = function(e) {
		return xpos = e.pageX / w;
	  };

	  window.requestAnimationFrame = (function() {
		return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(callback) {
		  return window.setTimeout(callback, 1000 / 60);
		};
	  })();

	  Confetti = (function() {
		function Confetti() {
		  this.style = COLORS[~~range(0, 5)];
		  this.rgb = "rgba(" + this.style[0] + "," + this.style[1] + "," + this.style[2];
		  this.r = ~~range(2, 10);
		  this.r2 = 140 * this.r;
		  this.replace();
		}

		Confetti.prototype.replace = function() {
		  this.opacity = 0;
		  this.dop = 0.01 * range(1, 4);
		  this.x = range(-this.r2, w - this.r2);
		  this.y = range(-20, h - this.r2);
		  this.xmax = w - this.r;
		  this.ymax = h - this.r;
		  this.vx = range(0, 2) + 8 * xpos - 5;
		  return this.vy = 0.3 * this.r + range(-1, 1);
		};

		Confetti.prototype.draw = function() {
		  var ref;
		  this.x += this.vx;
		  this.y += this.vy;
		  this.opacity += this.dop;
		  if (this.opacity > 1) {
			this.opacity = 1;
			this.dop *= -1;
		  }
		  if (this.opacity < 0 || this.y > this.ymax) {
			this.replace();
		  }
		  if (!((0 < (ref = this.x) && ref < this.xmax))) {
			this.x = (this.x + this.xmax) % this.xmax;
		  }
		  return drawCircle(~~this.x, ~~this.y, this.r, this.rgb + "," + this.opacity + ")");
		};

		return Confetti;

	  })();

	  confetti = (function() {
		var j, ref, results;
		results = [];
		for (i = j = 1, ref = NUM_CONFETTI; 1 <= ref ? j <= ref : j >= ref; i = 1 <= ref ? ++j : --j) {
		  results.push(new Confetti);
		}
		return results;
	  })();

	  window.step = function() {
		var c, j, len, results;
		requestAnimationFrame(step);
		context.clearRect(0, 0, w, h);
		results = [];
		for (j = 0, len = confetti.length; j < len; j++) {
		  c = confetti[j];
		  results.push(c.draw());
		}
		return results;
	  };

	  step();

	}).call(this);

});
