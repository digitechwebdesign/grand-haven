<?php
/*
Template Name: Blanco Vista
*/
get_header();

/*global $wpdb;
$table = 'bv_home_list';
$table_nei = 'bv_neighborhood_list';

$sql = "SELECT * FROM $table WHERE post_type = 'homes'";
$sql .= (!empty($where) ? " WHERE " . $where : '') . $order_by;

$home_result = $wpdb->get_results($sql);
$home_total = count($home_result);*/


$args = array(
    'post_type' => 'homes',
    'posts_per_page' => 200
);

$args['tax_query'] = array(
    'relation' => 'AND',
    array(
        'taxonomy' => 'community',
        'field'    => 'slug',
        'terms'    => array( 'Blanco Vista' ),
    )
);

$the_query = new WP_Query( $args );
?>

<div class="clear"></div>

<div class="inside-page row">

    <div class="c cx8">



            <!-- Title
            ============================================= -->
            <!-- <section class="page-header">
                <div class="glitch sunbeam topmargin"></div>
                <h1 class="topmargin-sm"><?php the_title(); ?></h1>
                <img class="bv-logo" src="<?php echo images(); ?>blanco-vista.png" alt="Blanco Vista">
            </section> -->


            <!-- Home Results
            ============================================= -->
            <section class="home-results topmargin">

                <?php if ( $the_query->have_posts() ) { ?>
                    <div class="title">
                        Currently Displaying <span><?php echo $the_query->found_posts; ?></span> Homes Available Now
                        <div class="glitch-m bluewarpaint topmargin-xsm"></div>
                    </div>

                    <?php
                    // Set the number so the correct forms will load :)
                    $homeNum = 1;
                    ?>

                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <?php $the_thumb = get_field("thumbnail"); ?>

                        <!-- Lightbox Form -->
                        <div style="display:none">
                            <div id="inquire-now<?php echo $homeNum; ?>" class="inquire-form">
                                <form action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
                                <input type=hidden name="oid" value="00D37000000Jkkf">
                                <input type=hidden name="retURL" value="<?php echo site_url(); ?>/blancovista-thank-you/">
                                <input type="hidden" name="Campaign_ID" value="70137000000DgE1" />
                                <input type="hidden" name="member_status" value="Responded" />
                                <input type="hidden" name="Brookfield_Residential_c" value="1" />

                                <label for="first_name">First Name*</label><input  id="first_name" maxlength="40" name="first_name" type="text" required /><br>

                                <label for="last_name">Last Name*</label><input  id="last_name" maxlength="80" name="last_name" type="text" required /><br>

                                <label for="phone">Phone</label><input  id="phone" maxlength="40" name="phone" type="text" /><br>

                                <label for="mobile">Mobile</label><input  id="00N37000004iAur" maxlength="40" name="00N37000004iAur" type="text" onkeydown="formatPhoneOnEnter(this, event);" /><br>

                                <label for="email">Email*</label><input  id="email" maxlength="80" name="email" type="text" required /><br>

                                <label for="zip">Zip*</label><input  id="zip" maxlength="20" name="zip" type="text" required /><br>

                                <span>Notes</span><textarea  id="00N37000004iAy0EAE" name="00N37000004iAy0EAE" rows="10" wrap="soft">I am interested in <?php the_title(); ?> for the price of $<?php echo get_field("price"); ?> - located in the neighborhood of <?php echo get_field("neighborhood"); ?> in the city of <?php echo get_field("city"); ?>.</textarea><script>CKEDITOR.timestamp = '4.4.6.2';
                                var protocolAndHost = window.location.protocol + '//' + window.location.host;

                                var editor = CKEDITOR.replace('00N37000004iAy0EAE', {
                                removePlugins : 'elementspath,maximize,image,tabletools,liststyle,contextmenu,resize',
                                baseHref : protocolAndHost + '/ckeditor/ckeditor-4.x/rel/',
                                customConfig : '/ckeditor/ckeditor-4.x/rel/sfdc-config.js',
                                height : '425',
                                bodyId : '00N37000004iAy0EAE_rta_body',
                                toolbar : 'SalesforceBasic',
                                sfdcLabels :{CkeMediaEmbed : { iframeMissing : 'Invalid &lt;iframe&gt; element. Please use valid code from the approved sites.', subtitle : 'Paste &amp;lt;iframe&amp;gt; code here:', description : 'Use &lt;iframe&gt; code from DailyMotion, Vimeo, and Youtube.', title : 'Embed Multimedia Content', exampleTitle : 'Example:', example : '\n            \n                &lt;iframe width=\&quot;560\&quot; height=\&quot;315\&quot; src=\&quot;https://www.youtube.com/embed/KcOm0TNvKBA\&quot; frameborder=\&quot;0\&quot; allowfullscreen&gt;&lt;/iframe&gt;\n            \n        '}, CkeImagePaste : { CkeImagePasteWarning : 'Pasting an image is not working properly with Firefox, please use [Copy Image location] instead.'}, CkeImageDialog : { infoTab_desc_info : 'Enter a description of the image for visually impaired users', uploadTab_desc : 'Description', defaultImageDescription : 'User-added image', uploadTab_file_info : 'Maximum size 1 MB. Only png, gif or jpeg', uploadTab_desc_info : 'Enter a description of the image for visually impaired users', imageUploadLimit_info : 'Max number of upload images exceeded', btn_insert_tooltip : 'Insert Image', httpUrlWarning : 'Are you sure you want to use an HTTP URL? Using HTTP image URLs may result in security warnings about insecure content. To avoid these warnings, use HTTPS image URLs instead.', title : 'Insert Image', error : 'Error:', uploadTab : 'Upload Image', wrongFileTypeError : 'You can insert only .gif .jpeg and .png files.', infoTab_url : 'URL', infoTab : 'Web Address', infoTab_url_info : 'Example: http://www.mysite.com/myimage.jpg', missingUrlError : 'You must enter a URL', uploadTab_file : 'Select Image', btn_update_tooltip : 'Update Image', infoTab_desc : 'Description', btn_insert : 'Insert', btn_update : 'Update', btn_upadte : 'Update', invalidUrlError : 'You can only use http:, https:, data:, //, /, or relative URL schemes.'}, sfdcSwitchToText : { sfdcSwitchToTextAlt : 'Use plain text'}},
                                contentsCss: ['/ckeditor/ckeditor-4.x/rel/contents.css', '/sCSS/34.0/sprites/1438195776000/Theme3/default/base/CKEditor.css', '/sCSS/34.0/sprites/1438195776000/Theme3/default/base/HtmlDetailElem.css'],
                                disableNativeSpellChecker : true,
                                language : 'en-us',
                                allowIframe : false,
                                sharedSpaces : { top : 'cke_topSpace', bottom : ' cke_bottomSpace' }
                                ,filebrowserImageUploadUrl : '/_ui/common/request/servlet/RtaImageUploadServlet'
                                });

                                </script><br>

                                <input type="submit" name="submit" value="Inquire Now">

                                </form>
                            </div>
                        </div> <!-- /Lightbox Form -->

                        <!-- Home Block -->
                        <div class="home-block no-description display-this">
                            <div class="image" style="background-image: url('<?php echo $the_thumb; ?>');"><a href="<?php echo $the_thumb; ?>" class="fancy-image blue" title="<?php the_title(); ?>"></a></div>
                            <div class="content">
                                <table>
                                    <tr class="block-row">
                                        <td colspan="2"><?php the_title(); ?><span class="stories"><?php echo get_field("stories"); ?> Story Home</span></td>
                                        <td colspan="2"><span class="results-price">$<?php echo get_field("price"); ?></span></td>
                                    </tr>
                                     <tr class="block-row">
                                        <td><span class="results-sqft"><?php echo get_field("square_feet"); ?></span> sq-ft</td>
                                        <td><div class="garage-icn"></div><span class="results-garage"><?php echo get_field("garages"); ?></span></td>
                                        <td><div class="bed-icn"></div><span class="results-beds"><?php echo get_field("bedrooms"); ?></span></td>
                                        <td><div class="bath-icn"></div><span class="results-bath"><?php echo get_field("full_baths"); ?></span></td>
                                    </tr>
                                     <tr class="block-row">
                                        <td colspan="2"><a href="https://www.google.com/maps/place/3016 Sand Post Place San Marcos, Texas 78666" target="_blank"><div class="pin-icn"></div><span><?php echo get_field("neighborhood"); ?></span><span><?php echo get_field("city"); ?></span></a></td>
                                        <td colspan="2"><a id="open-form<?php echo $homeNum; ?>" class="fancy-form">Inquire Now</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div><!-- /home-block -->

                        <?php $homeNum++ ?>
                    <?php endwhile; ?>

                    <div class="link">
                        <a class="load-more" href="#">Load More</a>
                        <div class="glitch-m mist-d topmargin-sm"></div>
                    </div>

                    <?php wp_reset_postdata(); ?>
                    <?php
                } else { ?>
                    <div class="title">
                        Sorry, No Results Were Found
                        <div class="glitch-m bluewarpaint"></div>
                    </div>
                <?php } ?>

            </section> <!-- /home-results -->


            <!-- <?php the_content(); ?> -->


    </div> <!-- /c cx8" -->
    <div class="clear"></div>
</div> <!-- /inside-page row -->
<?php

get_footer();

?>
