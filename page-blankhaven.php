<?php
/*
Template Name: Blank Haven
*/
get_header('grandhaven');
?>

<div style="min-height: 80vh; padding-top: 20px; text-align: center; font-size: 24px;">

<div class="inside-page row">

    <div class="c cx8">
    
        <?php while ( have_posts() ) : the_post(); ?>

            <!-- Title
            ============================================= -->
            <section class="page-header">
                <div class="glitch sunbeam topmargin"></div>
                <h1 style="font-size: 24px;" class="topmargin-sm"><?php the_content(); ?></h1>
            </section>
            
            <!-- Filter Search
            ============================================= -->
            <section class="search-hero">
                <div class="overlay"></div>
            </section>
            
            <div class="link topmargin-sm">
                <a class="button-main topmargin-sm" href="<?php echo site_url(); ?>/grand-haven-homes/">Back to Browsing</a>
            </div>

            <!-- <?php the_content(); ?> -->
            
        <?php endwhile; ?>
    </div> <!-- /c cx8" -->
    <div class="clear"></div>
</div> <!-- /inside-page row -->
</div>

<?php
get_footer('grandhaven');

?>