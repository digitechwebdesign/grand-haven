<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!--
                   *** ### ### ***
               *##                 ##*
           *##                         ##*
        *##                               ##*
      *##                                   ##*
    *##                                       ##*
   *##                                         ##*
  *##                                           ##*
 *##                      +                      ##*
 *##                Handcrafted by               ##*
 *##             digiTech Web Design             ##*
 *##                  ++++++++                   ##*
 *##                     <3                      ##*
  *##                                           ##*
   *##                                         ##*
    *##                                       ##*
      *##                                   ##*
        *#                                ##*
           *##                         ##*
               *##                 ##*
                   *** ### ### ***

      https://www.digitechwebdesignaustin.com/
                        <?php echo date("Y", time()); ?>


-->
 	<script src="//cdn.optimizely.com/js/2784180426.js"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="msvalidate.01" content="D236E7BFE3EAEA43A7D222C522C11934" />
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<title><?php
        global $page, $paged;
        
        $title_out =  wp_title( '|', true, 'right' );
        if (substr($title_out, -1) == "|")
        {
            $title_out = substr($title_out, 0, -2);
        }
        echo $title_out;
        
    ?></title>
    <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jquery.min.js'></script>
    <?php /*
    <script type="text/javascript">
	var brookfieldglobalnav_url_facebook = 'https://www.facebook.com/pages/Brookfield-Residential-Texas/1555935244646812';
        var brookfieldglobalnav_url_twitter = 'https://twitter.com/brookfieldtexas';
        var brookfieldglobalnav_url_pinterest = 'https://www.pinterest.com/BrookfieldTexas/';
    </script>
    <script src="http://alberta.brookfieldresidential.com/globalnav/js/brookfieldheader.min.css.js"></script>
    <script src="http://alberta.brookfieldresidential.com/globalnav/js/brookfieldheader.min.js"></script>
    */ ?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2AQeQfxdh3AklRoY2Y7WGam0AtOk8l5I&libraries=places"></script>
    
    <?php
        wp_head();
    ?>
    <link rel="icon" type="image/png" href="/favicon.png">
</head>

<body <?php body_class(); ?>>

<div class="loaderOverlay">
    <div class="wrapper">
    <ul class="container">
      <li>
        <div class="loader center"><span></span></div>
      </li>
    </ul>
</div>

</div>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-K3Q53X"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K3Q53X');</script>
<!-- End Google Tag Manager -->
    
    <div id="header">
        <div class="container">
            <div class="logo">
                <a href="/"><img src="<?php echo images(); ?>logo.jpg" alt="Brookfield Residential"/></a>
            </div>
            <div id="main-menu" class="menu" role="menu">
                <div class="responsive-button"><img src="<?php images(); ?>menu-button.png"/> <span>Menu</span></div>
                <?php wp_nav_menu( array('menu' => 'primary' )); ?>
                <script type="text/javascript">
                    jQuery(".menu-item-has-children").click(function() {
                        var parent_id = jQuery(this).attr("id");
                        
                        if (jQuery(".responsive-button").css("display") != "block") {
                            jQuery("#main-menu .sub-menu").hide();
                            
                            if (jQuery("#"+parent_id+" .sub-menu").css("display") != "block") {
                                jQuery("#"+parent_id+" .sub-menu").show();
                            } else {
                                jQuery("#"+parent_id+" .sub-menu").hide();
                            }
                        }
                    });
                    
                    jQuery(".responsive-button").click(function() {
                        if (jQuery("#menu-main-navigation").css("display") != "block") {
                            jQuery(".responsive-button span").hide();
                            jQuery("#menu-main-navigation").show();
                        } else {
                            jQuery("#menu-main-navigation").hide();
                            jQuery(".responsive-button span").show();
                        }
                    });
                </script>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    
    <div class="container">