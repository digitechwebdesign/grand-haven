
<div id="footer">
    <div class="breaker"><img src="<?php echo images(); ?>spacer.gif" style="width: 35px; height: 35px;" alt=""/></div>
    <div class="container">
        <div id="equal-housing-logo" class="floatleft"><img src="<?php echo images(); ?>equal-housing-opportunity.png" style="width: 81px; height: 73px;" alt=""/></div>
        &copy; <?php echo date("Y", time()); ?> Brookfield Properties Inc. All rights reserved.<br/><br/>
        <div class="disclaimer">
            The total number, layout, design and location of homes; unit mix; and the location, design
            and layout of recreational facilities may be changed due to a number of circumstances,
            including governmental requirements and market demand. Dimensions are approximate in
            nature and are not intended for final reference. Brookfield Residential and its affiliates
            reserve the right to change homesites, floorplans and home prices and make modifications in
            materials and specifications at any time without prior notice.
        </div>
        <ul class="footer-links">
            <li><a href="/realtors">Realtors</a></li>
            <li><a href="http://www.brookfieldresidential.com/" target="_blank">Corporate</a></li>
            <li><a href="http://www.brookfieldresidential.com/investor-relations/shareholder-information" target="_blank">Investor Relations</a></li>
            <li><a href="http://www.brookfieldresidential.com/careers/opportunities" target="_blank">Careers</a></li>
            <li><a href="http://www.brookfieldresidential.com/privacy-policy" target="_blank">Privacy</a></li>
            <li><a href="http://www.brookfieldresidential.com/terms-of-use" target="_blank">Terms</a></li>
        </ul>
        <div class="clear"></div>
        <div class="logo">
            <div class="social-icons" style="padding-bottom: 20px;">
                <ul>
                    <li><a href="https://www.facebook.com/pages/Brookfield-Residential-Texas/1555935244646812" target="_blank"><img src="<?php echo images(); ?>icon-facebook.png" alt="Facebook"/></a></li>
                    <li><a href="https://twitter.com/brookfieldtexas" target="_blank"><img src="<?php echo images(); ?>icon-twitter.png" alt="Twitter"/></a></li>
                    <li><a href="https://www.pinterest.com/BrookfieldTexas/" target="_blank"><img src="<?php echo images(); ?>icon-pinterest.png" alt="Pinterest"/></a></li>
                    <?php /* <li><a href="#"><img src="<?php echo images(); ?>icon-houzz.png" alt="Houzz"/></a></li> */ ?>
                </ul>
                <div class="clear"></div>
            </div>
            <div><a href="/about-us/"><img src="<?php echo images(); ?>logo.png" style="width: 147px; height: 41px;" alt="Brookfield Residential"/></a></div>
        </div>
    </div>
  <div class="pdf-modal">
	<a id="close-pdf"><i class="fa fa-times-circle-o fa-2x"></i></a>
	<a id="print-pdf"><i class="fa fa-print fa-2x"></i></a>
  	<?php  $pdf = get_field('modal-pdf');
		if($pdf){
		?>
	<iframe id="pdf-iframe" src="<?php echo $pdf;?>"></iframe>
  <?php
	}
	?>
  </div>
</div>
    <?php

        wp_footer();

    ?>
<script>
$("#imglinkgallery3").bind("click", function(){
	ga('send', 'event', 'Homepage', 'Click', 'Homepage Slider Click');
});

  $("#the-form").submit(function(e){
  		var firstName = $("#first_name").val();
		if(firstName == ""){
		  	e.preventDefault();
			alert("Please enter your First name");
		}

		var lastName = $("#last_name").val();
		if(firstName == ""){
		  	e.preventDefault();
			alert("Please enter your Last name");
		}

		var email = $("#email").val();
		if(email == ""){
		  	e.preventDefault();
			alert("Please enter your Email");
		}
  });

  var windowWidth = $(window).innerWidth();

  if(windowWidth  >800){

  $("#open-pdf").click(function(e){
	e.preventDefault();
	$(".pdf-modal").animate({
		left: "0px",
		top: "30px",
	  }, 500 );
	//$(".pdf-modal").css("left", "0px");
	//$(".pdf-modal").css("top", "30px");
  });

  }

  $("#close-pdf").click(function(){
  	$(".pdf-modal").animate({
		left: "-5000px"
	  }, 500 );
  });

  $("#print-pdf").click(function(){

      var PDF = document.getElementById("pdf-iframe");
      PDF.focus();
      PDF.contentWindow.print();

  });


</script>
</body>
</html>
