module.exports = function(grunt) {
    require('jit-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Sass ------------- //
        sass: {
            dist: {
                options: {
                    style: 'compressed',
                    loadPath: require('node-neat').includePaths
                },
                files: {
                    "css/style.css": "sass/style.scss" // destination file and source file
                }
            }
        }, // sass

        // Uglify ------------- //
        uglify: {
            my_target: {
                files: {
                    'js/fancybox.min.js': ['js/fancybox.js'],
                },
            },
        }, // uglify

        // Copy ------------- //
        copy: {
            // Bower
                // Bourbon
                bourbon: {
                    cwd: 'bower_components/bourbon/app/assets/stylesheets',
                    src: '**/*',
                    dest: 'sass/bourbon',
                    expand: true
                },
                // Neat
                neat: {
                    cwd: 'bower_components/neat/app/assets/stylesheets',
                    src: '**/*',
                    dest: 'sass/neat',
                    expand: true
                },
                // jQuery
                jquery: {
                    cwd: 'bower_components/jquery/dist',
                    src: 'jquery.min.js',
                    dest: 'js',
                    expand: true
                }
            // bower
        }, // copy

        watch: {
            styles: {
                files: ['sass/**/*.scss'], // which files to watch
                tasks: ['sass'],
                options: {
                    nospawn: true
                }
            } // styles
        } // watch

    }); // config

    grunt.registerTask('default', ['sass', 'watch']);
};