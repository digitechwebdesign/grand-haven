<?php
/*
Template Name: Neighborhoods
*/
get_header('grandhaven');

/*global $wpdb;
$table = 'bv_home_list';
$table_nei = 'bv_neighborhood_list';

$sql = "SELECT * FROM $table WHERE post_type = 'homes'";
$sql .= (!empty($where) ? " WHERE " . $where : '') . $order_by;

$home_result = $wpdb->get_results($sql);
$home_total = count($home_result);*/


$args = array(
	'post_type' => 'homes',
	'posts_per_page' => 200
);

$args['tax_query'] = array(
	'relation' => 'AND',
	array(
		'taxonomy' => 'community',
		'field'    => 'slug',
		'terms'    => array( 'Grand Haven' ),
	)
);



$the_query = new WP_Query( $args );
?>

<!-- Fullwidth Title/Map
============================================= -->
<section class="gh-split">
	<div class="hero-intro">
		<div class="content">
			<img src="<?php bloginfo("template_url"); ?>/images/10year.png" alt="Ten Year Birthday Tour">
			<h2>We’ve received such a huge response and sold so many homes, we’re <span class="highlight">Extending</span> our celebration until July 15!</h2>
			<p>It’s our birthday, but you get the gifts! Join the celebration and receive special 10-year pricing on quick move-in homes in 18 beautiful communities. Hurry! Only a few of these homes remain in your favorite community. Take the tour before they’re all gone.</p>
		</div>
		<img class="cake" src="<?php bloginfo("template_url"); ?>/images/cake.png" alt="">
		<!-- <canvas id="world"></canvas> -->
	</div>
	<div class="gh-map">
		<?php include(TEMPLATEPATH . "/images/gh-map.svg"); ?>
	</div>
</section>

<!-- Filter Search
============================================= -->
<section class="filter-search">
	<div class="title"><span>Search For Homes</span></div>
	<div class="filters">

		<article>
			<div class="dropy thecommunity">
				<div class="dropy_title"><div class="title-container"><span>Community</span><div class="arrow"></div></div></div>
				<div class="dropy_content">
					<ul>
						<li><a class="dropy_header"><div class="header-container"><span>Community</span><div class="arrow"></div></div></a></li>
						<li><a>Any</a></li>
						<li><a>Abrantes</a></li>
						<li><a>Belterra</a></li>
						<li><a>Bryson</a></li>
						<li><a>Caliterra</a></li>
						<li><a>Cap Rock Estates</a></li>
						<li><a>Cimarron Hills</a></li>
						<li><a>Cottages at Abrantes</a></li>
						<li><a>Cottages at Crystal Falls</a></li>
						<li><a>Cottages at Northwoods</a></li>
						<li><a>Paloma Lake</a></li>
						<li><a>Pearson Place</a></li>
						<li><a>Rough Hollow</a></li>
						<li><a>Santa Rita Ranch</a></li>
						<li><a>Sarita Valley</a></li>
						<li><a>Summit at Rough Hollow</a></li>
						<li><a>Terra Colinas</a></li>
						<li><a>Travisso</a></li>
						<li><a>Water Oak</a></li>
					</ul>
				</div>
				<input type="hidden" id="community" name="community">
			</div>
		</article>

		<article>
			<div class="dropy">
				<div class="dropy_title"><div class="title-container"><span>City</span><div class="arrow"></div></div></div>
				<div class="dropy_content">
					<ul>
						<li><a class="dropy_header"><div class="header-container"><span>City</span><div class="arrow"></div></div></a></li>
						<li><a>Any</a></li>
						<li><a>Austin</a></li>
						<li><a>Cedar Park</a></li>
						<li><a>Dripping Springs</a></li>
						<li><a>Georgetown</a></li>
						<li><a>Lakeway</a></li>
						<li><a>Leander</a></li>
						<li><a>Liberty Hill</a></li>
						<li><a>Round Rock</a></li>
					</ul>
				</div>
				<input type="hidden" id="city" name="city">
			</div>
		</article>

		<article>
			<div class="dropy">
				<div class="dropy_title"><div class="title-container"><span>Min Price</span><div class="arrow"></div></div></div>
				<div class="dropy_content">
					<ul>
						<li><a class="dropy_header"><div class="header-container"><span>Min Price</span><div class="arrow"></div></div></a></li>
						<li><a>Any</a></li>
						<li><a>$325,000</a></li>
						<li><a>$375,000</a></li>
						<li><a>$425,000</a></li>
						<li><a>$475,000</a></li>
						<li><a>$525,000</a></li>
						<li><a>$575,000</a></li>
						<li><a>$625,000</a></li>
						<li><a>$675,000</a></li>
						<li><a>$725,000</a></li>
						<li><a>$775,000</a></li>
						<li><a>$825,000</a></li>
						<li><a>$875,000</a></li>
						<li><a>$925,000</a></li>
						<li><a>$950,000</a></li>
					</ul>
				</div>
				<input type="hidden" id="min-price" name="min-price">
			</div>
		</article>

		<article>
			<div class="dropy">
				<div class="dropy_title"><div class="title-container"><span>Max Price</span><div class="arrow"></div></div></div>
				<div class="dropy_content">
					<ul>
						<li><a class="dropy_header"><div class="header-container"><span>Max Price</span><div class="arrow"></div></div></a></li>
						<li><a>Any</a></li>
						<li><a>$325,000</a></li>
						<li><a>$375,000</a></li>
						<li><a>$425,000</a></li>
						<li><a>$475,000</a></li>
						<li><a>$525,000</a></li>
						<li><a>$575,000</a></li>
						<li><a>$625,000</a></li>
						<li><a>$675,000</a></li>
						<li><a>$725,000</a></li>
						<li><a>$775,000</a></li>
						<li><a>$825,000</a></li>
						<li><a>$875,000</a></li>
						<li><a>$925,000</a></li>
						<li><a>$975,000</a></li>
						<li><a>$1,000,000</a></li>
					</ul>
				</div>
				<input type="hidden" id="max-price" name="max-price">
			</div>
		</article>

		<article>
			<div class="dropy">
				<div class="dropy_title"><div class="title-container"><span>Min SQFT</span><div class="arrow"></div></div></div>
				<div class="dropy_content">
					<ul>
						<li><a class="dropy_header"><div class="header-container"><span>Min SQFT</span><div class="arrow"></div></div></a></li>
						<li><a>Any</a></li>
						<li><a>2,000</a></li>
						<li><a>2,500</a></li>
						<li><a>3,000</a></li>
						<li><a>3,500</a></li>
						<li><a>4,000</a></li>
						<li><a>4,500</a></li>
					</ul>
				</div>
				<input type="hidden" id="min-sqft" name="min-sqft">
			</div>
		</article>

		<article>
			<div class="dropy">
				<div class="dropy_title"><div class="title-container"><span>Garage</span><div class="arrow"></div></div></div>
				<div class="dropy_content">
					<ul>
						<li><a class="dropy_header"><div class="header-container"><span>Garage</span><div class="arrow"></div></div></a></li>
						<li><a>Any</a></li>
						<li><a>2</a></li>
						<li><a>3</a></li>
						<li><a>4</a></li>
					</ul>
				</div>
				<input type="hidden" id="garage" name="garage">
			</div>
		</article>

		<article>
			<div class="dropy">
				<div class="dropy_title"><div class="title-container"><span>Bath</span><div class="arrow"></div></div></div>
				<div class="dropy_content">
					<ul>
						<li><a class="dropy_header"><div class="header-container"><span>Bath</span><div class="arrow"></div></div></a></li>
						<li><a>Any</a></li>
						<li><a>2</a></li>
						<li><a>2.5</a></li>
						<li><a>3</a></li>
						<li><a>3.5</a></li>
						<li><a>4</a></li>
						<li><a>4.5</a></li>
						<li><a>5</a></li>
					</ul>
				</div>
				<input type="hidden" id="bath" name="bath">
			</div>
		</article>

		<article>
			<div class="dropy">
				<div class="dropy_title"><div class="title-container"><span>Beds</span><div class="arrow"></div></div></div>
				<div class="dropy_content">
					<ul>
						<li><a class="dropy_header"><div class="header-container"><span>Beds</span><div class="arrow"></div></div></a></li>
						<li><a>Any</a></li>
						<li><a>3</a></li>
						<li><a>4</a></li>
						<li><a>5</a></li>
					</ul>
				</div>
				<input type="hidden" id="beds" name="beds">
			</div>
		</article>

	</div>
</section> <!-- /filter-search -->


<div class="container-full">
	<div class="inside-page row">
		<div class="c cx8">

			<!-- Home Results
			============================================= -->
			<section class="home-results">

				<?php if ( $the_query->have_posts() ) { ?>
					<div class="title">
						Currently Displaying <span><?php echo $the_query->found_posts; ?></span> Quick Move-ins
						<div class="glitch-m bluewarpaint topmargin-xsm"></div>
					</div>

					<!-- Lightbox Form -->
					<div style="display:none">
						<div id="inquire-now" class="inquire-form">
							<h3>We're celebrating 10 years of building luxury homes in Austin's best communities and you get the gifts! Hurry! Offer ends May 31st.</h3>
							<?php gravity_form( 10, false, false, false, '', false ); ?>
						</div>
					</div> <!-- /Lightbox Form -->

					<?php $homeNum = '1'; ?>

					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

						<?php $the_thumb = get_field("thumbnail"); ?>

						<!-- Home Block -->
						<div class="home-block display-this">
							<div class="image" style="background-image: url('<?php echo $the_thumb; ?>');">
								<a href="<?php echo $the_thumb; ?>" class="fancy-image" title="<?php the_title(); ?>"></a>
								<?php $virtualTour = get_field("virtual_tour");
									if ($virtualTour) { ?>
								<a href="<?php echo $virtualTour; ?>" target="_blank" class="virtual-tour">
									<img src="<?php echo get_template_directory_uri(); ?>/images/home-icn.svg" class="home" alt="">
									<span>Take Virtual Tour</span>
									<img src="<?php echo get_template_directory_uri(); ?>/images/rt-arrow.svg" alt="" class="rt-arrow">
								</a>
								<?php } ?>
							</div>
							<div class="content">
								<table>
									<tr class="block-row">
										<td colspan="2"><?php the_title(); ?><span class="stories"><?php echo get_field("stories"); ?> Story Home</span></td>
										<td colspan="2"><span class="was-price"><?php echo get_field("was_price"); ?></span><span class="results-price">$<?php echo get_field("price"); ?></span></td>
									</tr>
									 <tr class="block-row">
										<td><span class="results-sqft"><?php echo get_field("square_feet"); ?></span> sq-ft</td>
										<td><div class="garage-icn"></div><span class="results-garage"><?php echo get_field("garages"); ?></span></td>
										<td><div class="bed-icn"></div><span class="results-beds"><?php echo get_field("bedrooms"); ?></span></td>
										<td><div class="bath-icn"></div><span class="results-bath"><?php echo get_field("full_baths"); ?></span></td>
									</tr>
									 <tr class="block-row">
										<td colspan="2">
											<a href="https://www.google.com/maps/place/<?php the_title(); ?>, <?php echo get_field("city"); ?>, TX <?php echo get_field("zip"); ?>" target="_blank">
												<div class="pin-icn"></div>
												<span class="community"><?php echo get_field("neighborhood"); ?></span>
												<span class="city"><?php echo get_field("city"); ?></span>
											</a>
										</td>
										<td colspan="2">
											<a id="fancy-form" class="fancy-form home-form<?php echo $homeNum; ?>">Discover Additional Savings</a>
										</td>
									</tr>
									<tr class="block-row">
										<td colspan="4"><?php the_excerpt(); ?></td>
									</tr>
								</table>
							</div>
						</div><!-- /home-block -->

						<script>
							$('.home-form<?php echo $homeNum; ?>').click(function(){
								$('.textarea').html('I am interested in <?php the_title(); ?> for the price of $<?php echo get_field("price"); ?> - located in the neighborhood of <?php echo get_field("neighborhood"); ?> in the city of <?php echo get_field("city"); ?>.');
							});
						</script>

						<?php $homeNum++ ?>

					<?php endwhile; ?>

					<div class="link">
						<a class="load-more" href="#">Load More</a>
						<div class="glitch-m mist-d topmargin-sm"></div>
						<a href="http://www.grandhavenhomes.net/" class="our-site" target="_blank">See Our Full Site</a>
					</div>

					<?php wp_reset_postdata(); ?>
					<?php
				} else { ?>
					<div class="title">
						Sorry, No Results Were Found
						<div class="glitch-m bluewarpaint"></div>
					</div>
				<?php } ?>

			</section> <!-- /home-results -->

				<!-- <?php the_content(); ?> -->

		</div> <!-- /c cx8" -->
		<div class="clear"></div>
	</div> <!-- /inside-page row -->
</div> <!-- /container -->
<?php

get_footer('grandhaven');

?>
