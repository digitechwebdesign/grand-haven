<?php

/**
 * Enqueue scripts and styles.
 */
// wp_enqueue_style( 'brookfield-style', get_stylesheet_uri() );
wp_enqueue_style( 'brookfield-style', get_template_directory_uri() . '/css/style.css' );
wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/js/fancybox.min.js', array(), '20150930', true );
wp_enqueue_script( 'magnific-opup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array(), '20151007', true );
wp_enqueue_script( 'brookfield-scripts', get_template_directory_uri() . '/js/scripts.js', array(), '20150930', true );

function images() {
    echo get_template_directory_uri() . '/images/';
}

function do_theme_setup() {
    register_nav_menus( array( 'primary' => __( 'Primary Menu', 'brookfield' ) ) );
}

add_action( 'after_setup_theme', 'do_theme_setup' );

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
    add_post_type_support( 'page', 'excerpt' );
}

function show_homes($title = "Home plans", $exclude = "", $tax = "") {
    // Select current post id and remove it from the listing.
    
    $args = array(
        'post_type' => 'homes'
    );
    
    if($exclude) {
        $args['post__not_in'] = array( $exclude );
    }
    
    if($tax) {
        $args['tax_query'] = array(
            'relation' => 'AND',
            array(
                'taxonomy' => 'community',
                'field'    => 'slug',
                'terms'    => array( $tax ),
            )
    );
    }
    
    $the_query = new WP_Query( $args );
    
    if ( $the_query->have_posts() ) { ?>
        <div class="lato uppercase gray bold home-plans"><?php echo $title; ?></div>
        <div style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; height: 5px; overflow: hidden; margin-bottom: 20px; margin-top: 20px;"></div>
        <ul id="additional-home-plans">
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <?php $the_thumb = get_field("thumbnail"); ?>
            <li>
                <div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                <div class="playfair "><em><?php echo get_field("square_feet"); ?> square feet</em></div>
                <div class="playfair "><em>
                    <?php echo get_field("bedrooms"); ?> bedrooms,
                    <?php echo get_field("full_baths"); ?> baths,
                    <?php echo get_field("garages"); ?> car garage
                </em></div>
                <div class="thumbnail"><a href="<?php the_permalink(); ?>"><img src="<?php echo $the_thumb; ?>" alt="<?php the_title(); ?>"/></a></div>
            </li>
        <?php endwhile; ?>
        
        <div>
            <img src="<?php echo images(); ?>curly-mark.jpg" alt="" style="margin: 0 auto; margin-top: 20px;"/>
        </div><br/>
        
        <?php wp_reset_postdata(); ?>
        </ul>
        <?php
    }
}
?>