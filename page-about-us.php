<?php

get_header();

?>

<div class="clear"></div>

<div class="inside-page row">

    <div id="about-us" class="c cx8">
    
        <?php
            // Start the loop.
            while ( have_posts() ) : the_post();
                ?>
                
           <div class="floatright" style="margin-left: 20px;"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/videoseries?list=PLgJ38Y_JL9yHOW5VjP5JKsaSbmbHGD9kK&amp;controls=0&amp;?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div>
                
                <h1><?php the_title(); ?></h1>
                <div class="h1bar">&nbsp;</div>
                
                <?php the_content(); ?>
                
                <?php
            endwhile;
        ?>
    
    </div>
    
    <div class="clear"></div>
    
    <div class="c cx8 about-us">
        <div class="column">
            <img src="<?php images(); ?>about-us-1.jpg"/>
            
            <div class="block">
                <h2>Making hearts in Texas</h2>
                <h3 class="lato">BEAT FASTER.</h3>
                <p>
                    We believe building your home should be like falling in love, and you should be wowed from the start. We're there with you for the entire experience &mdash; from the first meeting to the final champagne toast when you move in, you'll feel the chemistry.
                </p>
                <p>
                    <a href="/communities/northwoods-at-blanco-vista/">Explore our communities.</a>
                </p>
            </div>
        </div>
        <div class="column">
            <img src="<?php images(); ?>about-us-2.jpg"/>
            
            <div class="block">
                <h2>Slip into something comfortable.</h2>
                <h3 class="lato">YOUR NEW HOME.</h3>
                <p>
                    Our experienced design and builder teams focus on what you want and need in your new home, whether it's incorporating the latest trends or creating a timeless living space. We'll ensure a seamless process to make your dream home come true.
                </p>
                <p>
                    <a href="/homes/residence-one/">Find new construction homes.</a>
                </p>
            </div>
        </div>
        <div class="column">
            <img src="<?php images(); ?>about-us-3.jpg"/>
            
            <div class="block">
                <h2>WE'RE SERIOUS</h2>
                <h3 class="lato">ABOUT SERVICE.</h3>
                <p>
                    Exceptional customer service is one of the cornerstones of our business. We're dedicated to making your experience with Brookfield Residential truly outstanding.
                </p>
                <p>
                    <a href="/contact-us/">Let's connect.</a>
                </p>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    
</div>

<?php

get_footer();

?>