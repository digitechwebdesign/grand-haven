<?php

get_header();

if(have_posts()) {
    while(have_posts()) {
        the_post();
        ?>
        
        <h2><a href="<?php the_permalink(); ?>"><?php echo wp_strip_all_tags( get_the_title(), true ); ?></a></h2>
        <p><?php the_excerpt(); ?></p>
        
        <?php
    }
}

get_footer();

?>