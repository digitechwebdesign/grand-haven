<?php

get_header();

?>

<div class="clear"></div>

<div class="inside-page row">

    <div class="c cx5-8">
        
        <div class="contact-us">
            <h1 class="playfair">LET'S CONNECT</h1>
            <div class="h1bar">&nbsp;</div>
            <div class="lato"><em>We're excited to hear from you.</em></div>
            <div class="lato">We can't wait to introduce you to <strong>THE BEST PLACE TO CALL HOME.</strong></div>
        </div>
        
        <?php echo FrmFormsController::get_form_shortcode(array('id' => 9, 'title' => false, 'description' => false)); ?>
        
        <div class="clear"></div>
    
    </div>
    
    <div class="c cx3-8">
        <div class="contact-us-block">
            
            <span class="sign-up">Sign up today and receive an invitation to our <strong>Private Preview Party</strong> at Retreat at Dripping Springs!</span><br/><br/>
            
            Contact us at:<br/>
            <a href="tel:888-277-2201">888-277-2201</a><br/>
            <a href="mailto:brookfieldtx@brookfieldrp.com">brookfieldtx@brookfieldrp.com</a>
            
            <div>
                <img src="<?php echo images(); ?>curly-mark.png" alt="" style="margin: 0 auto; margin-top: 20px;"/>
            </div>
        </div>
    </div>

    <div class="clear"></div>
    
</div>

<?php

get_footer();

?>