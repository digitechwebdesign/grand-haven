<?php

get_header();

?>

<h2>404 - Page Not Found</h2>
<p>The resource you are trying to find cannot be found.</p>

<?php

get_footer();

?>