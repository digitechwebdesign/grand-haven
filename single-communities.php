<?php

get_header();

?>

<div class="clear"></div>

<div class="inside-page row">

    <div class="c cx3">
    
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
            $map_location = get_field("map_coordinates");
            
            ?>
            
            <h1><?php the_title(); ?></h1>
            <div class="single-subtitle">
                <div class="floatleft"><?php the_excerpt(); ?></div>
                <div class="floatright"><ul>
                    <li><a href="mailto:?subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>"><img src="<?php echo images(); ?>email-icon.jpg" alt="Email"/></a></li>
                    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&summary=<?php the_title(); ?>" target="_blank"><img src="<?php echo images(); ?>linkedin-icon.jpg" alt="Linked In"/></a></li>
                    <li><a href="http://www.facebook.com/sharer.php?s=100&p[title]=<?php echo urlencode(str_replace('&#038;', '&', get_the_title()));
                        
                        if (!empty($features))
                        {
			    ?>&p[summary]=<?php echo urlencode(str_replace('&#038;', '&', $features));
                        }
                        ?>&src=sp" target="_blank"><img src="<?php echo images(); ?>facebook-icon.jpg" alt="Facebook"/></a></li>
                        
                    <li><a href="http://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>" target="_blank"><img src="<?php echo images(); ?>twitter-icon.jpg" alt="Twitter"/></a></li><?php /*
                    <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><img src="<?php echo images(); ?>gplus-icon.jpg" alt="Google Plus"/></a></li> */ ?>
                </ul></div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            
            <div class="slideshow">
                <?php if (function_exists('slideshow')) {
		    //slideshow(true, "1", false, array());
		    
		    echo do_shortcode( get_field("slideshow_short_code") );
		} ?>
            </div>
            
            <div class="single-cta">
                &nbsp;
            </div>
            
            <h2><?php echo get_field("subtitle"); ?></h2>
            <?php the_content(); ?>
            
            <div class="iframe-container">
		<div id="map-canvas"></div>
		<?php /*
                <iframe src="https://maps.google.com/maps?&q=<?php
                    //echo str_replace(" ", "+", get_the_title() . " " . get_the_excerpt());
		    
		    echo $map_location['lat'].",".$map_location['lng'];
                ?>&output=embed" width="100%" height="370" frameborder="0" style="border:0"></iframe>
                
                <iframe src="https://www.google.com/maps/d/embed?mid=z9ZAQHt8IGhw.kFB8RFNGvJMc" width="100%" height="370" frameborder="0" style="border:0"></iframe>
                */ ?>
            </div>
	    
	    <script language="javascript">
		
		var map_location = new google.maps.LatLng(<?php echo $map_location['lat']; ?>, <?php echo $map_location['lng']; ?>);
		var marker;
		var map;
		
		function initialize() {
		    var mapOptions = {
			center: map_location,
			zoom: 8
		    };
		    
		    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		    
		    marker = new google.maps.Marker({
			map:map,
			draggable:false,
			position: map_location,
			title: "<?php the_title(); ?>"
		    });
		}
		
		google.maps.event.addDomListener(window, 'load', initialize);
	    </script>
	    
            <div class="community-logo floatleft">
                <? //<img src="images(); blanco-vista.jpg"/> ?>
		<?php
		    if(get_field("community_logo")) {
			?>
			<img src="<?php echo get_field("community_logo"); ?>" alt="<?php the_title(); ?>"/>
			<?php
		    }
		?>
            </div>
            
            <?php
            
        // End the loop.
        endwhile;
        ?>
        
        <div class="clear"></div>
    
    </div>
    
    <div class="c cx2">
        
        <?php show_homes("Home plans", "Grand Haven", $post->post_name); ?>
        
        <div class="clear"></div>
        
        <div class="more-info-block">
            
            <div class="head">
                <div class="top-left floatleft">
                    <div></div>
                </div>
                <div class="top-center floatleft">
                    <img src="<?php images(); ?>envelope.png" alt="">
                </div>
                <div class="top-right floatleft">
                    <div></div>
                </div>
                <div class="clear"></div>
            </div>
            
            <div class="clear"></div>
        
            <div class="center uppercase bold pink" style="margin-bottom: 20px;">Need more info?</div>
            
            <div class="clear"></div>
            
            <?php echo FrmFormsController::get_form_shortcode(array('id' => 7, 'title' => false, 'description' => false)); ?>
            
        </div>
    </div>

    <div class="clear"></div>
    
</div>

<?php

get_footer();

?>