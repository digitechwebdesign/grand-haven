<?php
get_header();

?>

<div class="clear"></div>

<div class="inside-page row">

    <div class="c cx3">
    
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
            $map_location = "";
            
            ?>
            
            <h1><?php the_title(); ?></h1>
            <div class="single-subtitle">
                <div class="floatleft"><?php the_excerpt(); ?></div>
                <div class="floatright"><ul>
                    <li><a href="mailto:?subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>"><img src="<?php echo images(); ?>email-icon.jpg" alt="Email"/></a></li>
                    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&summary=<?php the_title(); ?>" target="_blank"><img src="<?php echo images(); ?>linkedin-icon.jpg" alt="Linked In"/></a></li>
                    <li><a href="http://www.facebook.com/sharer.php?s=100&p[title]=<?php echo urlencode(str_replace('&#038;', '&', get_the_title()));
                        
                        if (!empty($features))
                        {
                          ?>&p[summary]=<?php echo urlencode(str_replace('&#038;', '&', $features));
                          
                        }
                        ?>&src=sp" target="_blank"><img src="<?php echo images(); ?>facebook-icon.jpg" alt="Facebook"/></a></li>
                        
                    <li><a href="http://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>" target="_blank"><img src="<?php echo images(); ?>twitter-icon.jpg" alt="Twitter"/></a></li>
                    <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><img src="<?php echo images(); ?>gplus-icon.jpg" alt="Google Plus"/></a></li>
                </ul></div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            
            <div class="slideshow">
                <?php if (function_exists('slideshow')) { slideshow(true, "1", false, array()); } ?>
            </div>
            
            <div class="single-cta">
                &nbsp;
            </div>
            
            <h2><?php echo get_field("subtitle"); ?></h2>
            <?php the_content(); ?>
            <?php /* <p><a href="#">SEE OUR NEW HOMES</a></p> */ ?>
            
            <div class="iframe-container">
                <?php /*
                <iframe src="https://maps.google.com/maps?&q=<?php
                    //echo "Blanco+Vista+Homes+by+Pacesetter+Homes+Texas";
                    echo str_replace(" ", "+", get_the_title() . " " . get_the_excerpt());
                ?>&output=embed" width="100%" height="370" frameborder="0" style="border:0"></iframe>
                */ ?>
                <iframe src="https://www.google.com/maps/d/embed?mid=z9ZAQHt8IGhw.kFB8RFNGvJMc" width="100%" height="370" frameborder="0" style="border:0"></iframe>
            </div>
            
            <div class="floatleft">
                <img src="<?php images(); ?>blanco-vista.jpg"/>
            </div>
            
            <?php
            
        // End the loop.
        endwhile;
        ?>
        
        <div class="clear"></div>
    
    </div>
    
    <div class="c cx2">
        <?php /*
        <div class="lato uppercase gray bold">Additional home plans</div>
        <div style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; height: 5px; overflow: hidden; margin-bottom: 20px; margin-top: 20px;"></div>
        <?php
            
            
            
        ?>
        <div>
            <img src="<?php echo images(); ?>curly-mark.jpg" alt="" style="margin: 0 auto; margin-top: 20px;"/>
        </div>
        
        <div class="clear"></div>
        */ ?>
        
        <div class="more-info-block">
            
            <div class="head">
                <div class="top-left floatleft">
                    <div></div>
                </div>
                <div class="top-center floatleft">
                    <img src="<?php images(); ?>envelope.png" alt="">
                </div>
                <div class="top-right floatleft">
                    <div></div>
                </div>
                <div class="clear"></div>
            </div>
            
            <div class="clear"></div>
        
            <div class="center uppercase bold pink" style="margin-bottom: 20px;">Need more info?</div>
            
            <div class="clear"></div>
            
            <?php echo FrmFormsController::get_form_shortcode(array('id' => 7, 'title' => false, 'description' => false)); ?>
            
        </div>
    </div>

    <div class="clear"></div>
    
</div>

<?php

get_footer();

?>