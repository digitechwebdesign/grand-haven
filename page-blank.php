<?php
/*
Template Name: Blank
*/
get_header();
?>

<div style="min-height: 80vh; padding-top: 20px; text-align: center; font-size: 24px;">

<div class="inside-page row">

    <div class="c cx8">
    
        <?php while ( have_posts() ) : the_post(); ?>

            <?php the_content(); ?>
            
        <?php endwhile; ?>
    </div> <!-- /c cx8" -->
    <div class="clear"></div>
</div> <!-- /inside-page row -->
</div>

<?php
get_footer();

?>